import { sapa, convert, filterData, checkScore } from "./libs/soal";

const myArgs = process.argv.slice(2);
const command = myArgs[0]

switch (command) {
    case 'sapa':
        let name = myArgs[1]
        console.log(sapa(name))
        break;

    case 'convert':
        const params = myArgs.slice(1);

        let [nama, domisili, umur] = params

        console.log(convert(nama, domisili, umur))
        break;

    case 'checkScore':        
        let element = myArgs[1].split(",")
        let obj = {}
        for (let i = 0; i < element.length; i++) {
        
            let koma = element[i].split(":");
            obj[koma[0]] =  koma[1]

        }
        console.log(obj)

        break;

    case 'filterData':
        let namaKelas = myArgs[1]
        console.log(filterData(namaKelas))
        break;

    default:
        break;
}