import Employee from "./employee"
import Student from "./student"
import fs from "fs"
import "core-js/stable"
import "regenerator-runtime/runtime"
import fsPromises from "fs/promises"

const path = "data.json"

class Bootcamp {
    static register(input) {
        let [name, password, role] = input.split(",")
        fs.readFile(path, (err, data) => {
            if (err) {
                console.log(err)
            }

            let existingData = JSON.parse(data)
            let employee = new Employee(name, password, role)
            existingData.push(employee)
            fs.writeFile(path, JSON.stringify(existingData), (err) => {
                if (err) {
                    console.log(err)
                } else {
                    console.log("berhasil register")
                }
            })
        })
    }

    static login(input) {
        let [name, password] = input.split(",")

        fsPromises.readFile(path).then((data) => {
            let employees = JSON.parse(data)

            let indexEmp = employees.findIndex((emp) => emp._name == name)

            if (indexEmp == -1) {
                console.log("Data tidak ditemukan")
            } else {
                let employee = employees[indexEmp]

                if (employee._password == password) {
                    employee._isLogin = true

                    employees.splice(indexEmp, 1, employee)
                    return fsPromises.writeFile(path, JSON.stringify(employees))
                } else {
                    console.log("Password yang dimasukan salah")
                }
            }
        })
            .then(() => {
                console.log("Berhasil Login")
            })
            .catch((err) => {
                console.log(err)
            })

    }

    // static addSiswa = async (input) => {
    //     let [name, trainer] = input.split(",")
    //     let dataRead = await fsPromises.readFile(path)
    //     let realData = JSON.parse(dataRead)
    //     let employee = new Employee(name, trainer)
    //     realData.push(employee)
    //     await fsPromises.writeFile(path, JSON.stringify(realData))
    //     console.log("Berhasil add siswa")

    // }
    static addSiswa = async (input) => {
        let [name, trainer] = input.split(",")
        let dataRead = await fsPromises.readFile(path)
        let realData = JSON.parse(dataRead)
        let employee = new Employee(trainer, '123456', 'trainer')
        employee.addS({'name':name})
        let newObj = {
            name: employee._name,
            password: employee._password,
            role: employee._role,
            isLogin: employee._isLogin,
            students: employee._students,
        }
        realData.push(newObj)
        await fsPromises.writeFile(path, JSON.stringify(realData))
        console.log("Berhasil add siswa")

    }
}



export default Bootcamp