class Student {
    constructor(name, trainer) {
        this._name = name
        this._trainer = trainer
    }

    get name() {
        return this._name
    }

    set name(name) {
        this._name = name
    }

    get trainer() {
        return this._trainer
    }

    set trainer(trainer) {
        this._trainer = trainer
    }

}

export default Student