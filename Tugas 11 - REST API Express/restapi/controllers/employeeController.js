const fs = require('fs')
const fsPromises = require('fs/promises')

const path = 'data.json'

class employeeController {
    static register(req, res) {
        // res.status(200).json({message: "testing"})
        fs.readFile(path, (err, data) => {
            if (err) {
                res.status(400).json({
                    error: "error data tidak terbaca"
                })
            } else {
                let existingData = JSON.parse(data)

                let { users } = existingData

                let { name, role, password } = req.body
                let newEmployee = { name, role, password, isLogin: false }

                users.push(newEmployee)

                let newData = { ...existingData, users }

                fs.writeFile(path, JSON.stringify(newData), (err) => {
                    if (err) {
                        req.status(400).json({ error: "error menyimpan data" })
                    } else {
                        res.status(201).json({
                            message: "Berhasil register"
                        })
                    }
                })
            }
        })
    }

    static findAlll(req, res) {
        fs.readFile(path, (err, data) => {
            if (err) {
                res.status(400).json({ error: "error membaca data" })
            } else {
                let realData = JSON.parse(data)
                res.status(200).json({
                    message: "Berhasil get karyawan",
                    data: realData.users
                },
                )
            }
        })
    }

    static login(req, res) {
        fsPromises.readFile(path)
            .then((data) => {
                let employees = JSON.parse(data)

                let { users } = employees

                let { name, password } = req.body

                let indexEmp = users.findIndex(user => user.name == name)

                // console.log(indexEmp)
                if (indexEmp == -1) {
                    res.status(404).json({ error: "data tidak tersedia" })
                } else {
                    let employee = users[indexEmp]

                    if (employee.password == password) {
                        employee.isLogin = true

                        users.splice(indexEmp, 1, employee)
                        return fsPromises.writeFile(path, JSON.stringify(employees))
                    } else {
                        res.status(400).json({ error: "password salah" })
                    }
                }

            })
            .then(() => {
                res.status(200).json({ error: "Berhasil login" })
            })
            .catch((err) => {
                console.log(err)
            })
    }

    static async addSiswa(req, res) {

        let { studentName, className } = req.body
        let employeesRaw = await fsPromises.readFile(path)
        let employees = JSON.parse(employeesRaw)

    
        let admin = employees.find((emp) => emp.role == "admin")

        if (!admin.isLogin) {
            console.log("not allowed to add siswa")
            return
        }


        let indexClass = employees.findIndex((emp) => emp.name == className)

        if(indexClass == -1) {
            res.status(404).json({error : "class tidak tersedia"})
        } else {
            let kelas = employees[indexClass]

            if (kelas.students) {
                kelas.students.push({name: studentName})

                // users.splice(indexEmp, 1, employee)
                // return fsPromises.writeFile(path, JSON.stringify(employees))
            } else {
                kelas.students = [{name: studentName}]
                // res.status(400).json({ error: "add siswa gagal" })
            }

            employees.splice(indexClass, 1, kelas)

            let write = await fsPromises.writeFile(path, JSON.stringify(employees))
            console.log("Berhasil add siswa")

        }



        // let [studentName, className] = req.body
        // let employeesRaw = await fsPromises.readFile(path)
        // let employees = JSON.parse(employeesRaw)

        // let admin = employees.find((emp) => emp.role == "admin")

        // if (!admin.isLogin) {
        //     console.log("not allowed to add siswa")
        //     return
        // }

        // let indexClass = users.findIndex(user => user.name == className)

        // if(indexEmp == -1) {
        //     res.status(404).json({error : "data tidak tersedia"})
        // } else {
        //     let class = employees[indexClass]

        // }
        // fsPromises.readFile(path)
        // .then((data) => {
        //     let employees = JSON.parse(data)

        //     let {users} = employees

        //     let {name, password} = req.body

        //     let indexEmp = users.findIndex(user => user.name == name)

        //     let admin = employees.find((emp) => emp.role == "admin")

        //     if (!admin.isLogin) {
        //         console.log("not allowed to add siswa")
        //         return
        //     }

        // if(indexEmp == -1) {
        //     res.status(404).json({error : "data tidak tersedia"})
        // } else {
        //     let employee = users[indexEmp]

        //     if(employee.role == admin) {
        //         employee.isLogin=true

        //         users.splice(indexEmp, 1, employee)
        //         return fsPromises.writeFile(path, JSON.stringify(employees))
        //     } else {
        //         res.status(400).json({error : "password salah"})
        //     }
        // }

    }
}

module.exports = employeeController