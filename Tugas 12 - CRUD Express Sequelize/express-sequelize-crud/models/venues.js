'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Venues extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Venues.init({
    name: {
      type : DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull : {
          msg: "Inputan name tidak boleh kososng"
        }
      }
    },
    address: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull : {
          msg: "Inputan address tidak boleh kososng"
        }
      }
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull : {
          msg: "Inputan phone tidak boleh kososng"
        }
      }
    }
  }, {
    sequelize,
    modelName: 'Venues',
  });
  return Venues;
};