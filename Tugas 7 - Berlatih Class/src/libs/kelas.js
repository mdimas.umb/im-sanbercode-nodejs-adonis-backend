class Kelas {
    constructor(name, level, instructor) {
        this._name = name
        this._level = level
        this._instructor = instructor
        this._students = []
    }

    get name() {
        return this._name
    }

    set name(name) {
        this._name = name
    }

    get level() {
        return this._level
    }

    set level(level) {
        this._level = level
    }

    get instructor() {
        return this._instructor
    }

    set instructor(instructor) {
        this._instructor = instructor
    }

    addStudents(objStudents) {
        this._students.push(objStudents)
    }
}

export default Kelas

