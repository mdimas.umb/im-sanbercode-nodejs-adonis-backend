import Kelas from './kelas'

class Bootcamp {
    constructor(name) {
        this._name = name
        this._classes = []
    }
    
    get name() {
        return this._name
    }

    set name(name) {
        this._name = name
    }

    get classes() {
        return this._classes
    }

    createClass(name, level, instructor) {
        let newClass = new Kelas(name, level, instructor)
        this._classes.push(newClass)
    }

    register(namaKelas, objStudent) {
        let carikelas = this._classes.find((kelas) => kelas.name === namaKelas)
        carikelas.addStudents(objStudent)
    }

}

export default Bootcamp;