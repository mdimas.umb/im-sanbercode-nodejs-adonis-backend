import Bootcamp from "./libs/bootcamp";
import Student from "./libs/student";

const sanber = new Bootcamp("sanbercode")

sanber.createClass("Laravel", "beginner", "abduh")
sanber.createClass("React", "beginner", "abdul")

console.log("============Jawab Soal No 1============")
console.log(sanber.classes)

console.log("\n============Jawab Soal No 2============")
let names = ["regi", "ahmad", "bondra", "iqbal", "putri", "rezky"]

names.map((nama, index) => {
    let newStud = new Student(nama)
    let kelas = sanber.classes[index % 2].name
    sanber.register(kelas, newStud)
})

sanber.classes.forEach(kelas => {
    console.log(kelas)
});

