// If-else
console.log("==========Jawaban Soal If-Else==========")
var nama = "John"
var peran = ""

if (nama == "" && peran == "") {
    console.log("Nama harus diisi!")
} else if (nama !== "") {
    if (peran == "") {
        console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`)
    } else if (peran == "Penyihir") {
        console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
        console.log(`Halo ${peran} ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`)
    } else if (peran == "Guard") {
        console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
        console.log(`Halo ${peran} ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`)
    } else if (peran == "Werewolf") {
        console.log(`Selamat datang di Dunia Werewolf, ${nama}`)
        console.log(`Halo ${peran} ${nama}, Kamu akan memakan mangsa setiap malam!`)
    } else {
        console.log("Peran tidak ditemukan!")
    }
}


// Switch Case
console.log("\n==========Jawaban Soal Switch Case==========")

var hari = 21;
var bulan = 1;
var tahun = 1945; 
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945'; 

var tanggal;
tanggal = hari // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
if (tanggal >= 1 && tanggal <= 31) {
    tanggal
} else {
    console.log(`Tanggal ${tanggal} tidak valid"`)
}

var bulan; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
if (bulan > 0 && bulan < 13) {
    bulan
} else {
    console.log(`Bulan ${bulan} tidak valid`)
}


var tahun; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)
if (tahun >= 1900 && tahun <= 2200) {
    tahun
} else {
    console.log(`Tahun ${tahun} tidak valid`)
}




switch (bulan) {
    case 1:
        console.log(hari + " Januari " + tahun)
        break;
    case 2:
        console.log(hari + " Februari " + tahun)
        break;
    case 3:
        console.log(hari + " Maret " + tahun)
        break;
    case 4:
        console.log(hari + " April " + tahun)
        break;
    case 5:
        console.log(hari + " Mei " + tahun)
        break;
    case 6:
        console.log(hari + " Juni " + tahun)
        break;
    case 7:
        console.log(hari + " Juli " + tahun)
        break;
    case 8:
        console.log(hari + " Agustus " + tahun)
        break;
    case 9:
        console.log(hari + " September " + tahun)
        break;
    case 10:
        console.log(hari + " Oktober " + tahun)
        break;
    case 11:
        console.log(hari + " November " + tahun)
        break;
    case 12:
        console.log(hari + " Desember " + tahun)
        break;
    default:
        console.log("Bulan tidak valid")
        break;
}