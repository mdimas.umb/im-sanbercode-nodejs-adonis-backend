import { schema, CustomMessages, rules } from '@ioc:Adonis/Core/Validator'
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { DateTime } from 'luxon'


export default class BookingValidator {
  constructor(protected ctx: HttpContextContract) {}

  /*
   * Define schema to validate the "shape", "type", "formatting" and "integrity" of data.
   *
   * For example:
   * 1. The username must be of data type string. But then also, it should
   *    not contain special characters or numbers.
   *    ```
   *     schema.string({}, [ rules.alpha() ])
   *    ```
   *
   * 2. The email must be of data type string, formatted as a valid
   *    email. But also, not used by any other user.
   *    ```
   *     schema.string({}, [
   *       rules.email(),
   *       rules.unique({ table: 'users', column: 'email' }),
   *     ])
   *    ```
   */
  public refs = schema.refs({
    allowedDate: DateTime.local().plus({ days: 2 })
  })

  public schema = schema.create({
    nama : schema.string({}, [
      rules.alpha({
        allow: ['space', 'underscore', 'dash']
      }),
      rules.minLength(5)
    ]),
    venue : schema.string({}, [
      rules.alphaNum({
        allow: ['space', 'underscore', 'dash']
      }),
    ]),
    tanggalBook : schema.date({format: 'yyyy-MM-dd HH:mm:ss'}, [
      rules.after(this.refs.allowedDate),
      // rules.before(this.refs.allowedDate)
    ]),
  })

  /**
   * Custom messages for validation failures. You can make use of dot notation `(.)`
   * for targeting nested fields and array expressions `(*)` for targeting all
   * children of an array. For example:
   *
   * {
   *   'profile.username.required': 'Username is required',
   *   'scores.*.number': 'Define scores as valid numbers'
   * }
   *
   */
  public messages: CustomMessages = {
    required : "inputan {{field}} tidak boleh kosong",
    "nama.minLength": "inputan {{field}} harus lebih dari 5",
    "rule.tanggalBook": "Booking harus 1 hari sebelum"
  }
}
