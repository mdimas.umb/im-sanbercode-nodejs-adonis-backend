import type { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
// import VenueValidator from "App/Validators/VenueValidator";
import Database from "@ioc:Adonis/Lucid/Database";

export default class VenuesController {
  public async index({ response }: HttpContextContract) {
    try {
      const datavenues = await Database
        .from("venues") // 👈 gives an instance of select query builder
        .select("*"); 
        
        response.ok({
          message: "Success menampilkan data",
          data: datavenues
        });
    } catch (error) {
      response.badRequest({
        message: "Gagal menampilkan data"
      })
    }
  }

  public async store({ response, request }: HttpContextContract) {
    try {
      await Database.table("venues") // 👈 gives an instance of insert query builder
        .insert({
          name: request.input("name"),
          address: request.input("address"),
          phone: request.input("phone"),
        });

      response.created({
        message: "Success simpan data",
      });
    } catch (error) {
      response.badRequest({
        message: "Gagal menyimpan data venues",
      });
    }
  }

  public async show({response, params}: HttpContextContract) {
    try {
      const datadetail = await Database
        .from('venues')
        .where('id', params.id)
        .first()

        response.ok({
          message: "Success menampilkan data",
          data: datadetail
        });
    } catch (error) {
      response.badRequest({
        message: "Gagal menyimpan data venues",
      });
    }
    
  }

  public async update({request, response, params}: HttpContextContract) {
    try {
      await Database
        .from('venues')
        .where('id', params.id)
        .update(
          { 
            name: request.input("name"),
            address: request.input("address"),
            phone: request.input("phone"),
          }
        )

        response.ok({
          message: "Success Update data",
        });
    } catch (error) {
      response.badRequest({
        message: "Gagal update data venues",
      });
    }
  }

  public async destroy({response, params}: HttpContextContract) {
    try {
      await Database
        .from('venues')
        .where('id', params.id)
        .delete()

        response.ok({
          message: "Success Delete data",
        });
    } catch (error) {
      response.badRequest({
        message: "Data gagal terhapus",
      });
    }

  }
}
