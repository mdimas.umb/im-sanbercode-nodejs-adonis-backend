import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from "@ioc:Adonis/Lucid/Database";


export default class FieldsController {
  public async index({response}: HttpContextContract) {
    try {
      const datafields = await Database
        .from("fields") // 👈 gives an instance of select query builder
        .select("*"); 
        
        response.ok({
          message: "Success menampilkan data",
          data: datafields
        });
    } catch (error) {
      response.badRequest({
        message: "Gagal menampilkan data"
      })
    }
  }

  public async store({ response, request}: HttpContextContract) {
    try {
      await Database.table("fields") // 👈 gives an instance of insert query builder
        .insert({
          name: request.input("name"),
          type: request.input("type"),
          venue_id: request.input("venue_id")
        });

      response.created({
        message: "Success simpan data",
      });
    } catch (error) {
      response.badRequest({
        message: "Gagal menyimpan data venues",
      });
    }
  }

  public async show({response, params}: HttpContextContract) {
    try {
      const datadetail = await Database
        .from('fields')
        .where('id', params.id)
        .first()

        response.ok({
          message: "Success menampilkan data",
          data: datadetail
        });
    } catch (error) {
      response.badRequest({
        message: "Gagal menyimpan data venues",
      });
    }
  }

  public async update({request, response, params}: HttpContextContract) {
    try {
      await Database
        .from('fields')
        .where('id', params.id)
        .update(
          { 
            name: request.input("name"),
            type: request.input("type"),
            venue_id: request.input("venue_id")
          }
        )

        response.ok({
          message: "Success Update data",
        });
    } catch (error) {
      response.badRequest({
        message: "Gagal update data venues",
      });
    }
  }

  public async destroy({response, params}: HttpContextContract) {
    try {
      await Database
        .from('fields')
        .where('id', params.id)
        .delete()

        response.ok({
          message: "Success Delete data",
        });
    } catch (error) {
      response.badRequest({
        message: "Data gagal terhapus",
      });
    }

  }
}
