import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import BookingValidator from 'App/Validators/BookingValidator'

export default class BookingsController {
    public async kirim ({request, response} : HttpContextContract) {
        try {
            const newBookings = await request.validate(BookingValidator)

            response.ok ({
                message : newBookings
            })

        } catch (error) {
            response.badRequest(error.messages)
        }
    }
}
