import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from "@ioc:Adonis/Lucid/Database";


export default class GenresController {
  public async index({response}: HttpContextContract) {
    try {
      const datagenres = await Database
        .from("genres") // 👈 gives an instance of select query builder
        .select("*"); 
        
        response.ok({
          message: "Success menampilkan data",
          data: datagenres
        });
    } catch (error) {
      response.badRequest({
        message: "Gagal menampilkan data"
      })
    }
  }

  public async store({response, request}: HttpContextContract) {
    try {
      await Database.table("genres") // 👈 gives an instance of insert query builder
        .insert({
          name: request.input("name")
        });

      response.created({
        message: "Success simpan data",
      });
    } catch (error) {
      response.badRequest({
        message: "Gagal menyimpan data genres",
      });
    }
  }

  public async show({response, params}: HttpContextContract) {
    try {
      const datadetail = await Database
        .from('genres')
        .where('id', params.id)
        .first()

        response.ok({
          message: "Success menampilkan data",
          data: datadetail
        });
    } catch (error) {
      response.badRequest({
        message: "Gagal manampilkan data genres",
      });
    }
  }

  public async update({request, response, params}: HttpContextContract) {
    try {
      await Database
        .from('genres')
        .where('id', params.id)
        .update(
          { 
            name: request.input("name"),
          }
        )

        response.ok({
          message: "Success Update data genre",
        });
    } catch (error) {
      response.badRequest({
        message: "Gagal update data genre",
      });
    }
  }

  public async destroy({response, params}: HttpContextContract) {
    try {
      await Database
        .from('genres')
        .where('id', params.id)
        .delete()

        response.ok({
          message: "Success Delete data",
        });
    } catch (error) {
      response.badRequest({
        message: "Data gagal terhapus",
      });
    }

  }
}
