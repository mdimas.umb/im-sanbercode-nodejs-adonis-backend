import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from "@ioc:Adonis/Lucid/Database";


export default class MoviesController {
  public async index({response}: HttpContextContract) {
    try {
      const datamovies = await Database
        .from("movies") // 👈 gives an instance of select query builder
        .select("*"); 
        
        response.ok({
          message: "Success menampilkan data",
          data: datamovies
        });
    } catch (error) {
      response.badRequest({
        message: "Gagal menampilkan data"
      })
    }
  }

  public async store({response, request}: HttpContextContract) {
    try {
      await Database.table("movies") // 👈 gives an instance of insert query builder
        .insert({
          title: request.input('title'),
          resume: request.input('resume'),
          release_date: request.input('release_date'),
          genre_id: request.input('genre')
        });

      response.created({
        message: "Success simpan data",
      });
    } catch (error) {
      response.badRequest({
        message: "Gagal menyimpan data movie",
      });
    }
  }

  public async show({response, params}: HttpContextContract) {
    try {
      const datadetail = await Database
        .from('movies')
        .where('id', params.id)
        .first()

        response.ok({
          message: "Success menampilkan data",
          data: datadetail
        });
    } catch (error) {
      response.badRequest({
        message: "Gagal manampilkan data movies",
      });
    }
  }

  public async update({request, response, params}: HttpContextContract) {
    try {
      await Database
        .from('movies')
        .where('id', params.id)
        .update(
          { 
            title: request.input('title'),
            resume: request.input('resume'),
            release_date: request.input('release_date'),
            genre_id: request.input('genre')
          }
        )

        response.ok({
          message: "Success Update data movie",
        });
    } catch (error) {
      response.badRequest({
        message: "Gagal update data movie",
      });
    }
  }

  public async destroy({response, params}: HttpContextContract) {
    try {
      await Database
        .from('movies')
        .where('id', params.id)
        .delete()

        response.ok({
          message: "Success Delete data",
        });
    } catch (error) {
      response.badRequest({
        message: "Data gagal terhapus",
      });
    }
  }
}
